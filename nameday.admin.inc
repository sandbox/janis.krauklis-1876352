<?php
/**
 * @file
 * Admin configuration page.
 */

/**
 * Shows nameday module configuration form.
 */
function nameday_form($form, &$form_state) {
  $uid = 0;
  $domain = $_SERVER['SERVER_NAME'];
  $domain = preg_replace('#^(http(s)?://)?w{3}\.#', '', $domain);

  $result = db_query('SELECT * FROM nameday LIMIT 1', array(':nid' => $uid));

  if ($result->rowCount() > 0) {
    foreach ($result as $row) {
      $api_key = $row->api_key;
      $country = $row->country;
      $list_radio = $row->show_row;
      $row_radio = $row->show_list;
    }
  }
  else {
    $api_key = '';
    $country = 'LV';
    $list_radio = 1;
    $row_radio = 0;
  }

  $form['api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#default_value' => $api_key,
    '#size' => 60,
    '#maxlength' => 60,
    '#description' => t('API Key hash.'),
    '#required' => TRUE,
  );
  unset($_SESSION['messages']['warning']);

  $postdata = http_build_query(array('body' => '{"jsonrpc" : "2.0","method" : "CheckApiKey","params" : {"user" : "","domain" : "' . $domain . '","apiKey" : "' . $api_key . '"},"id" : "' . $uid . '"}'));
  $options = array(
    'method' => 'POST',
    'data' => $postdata,
    'timeout' => 15,
    'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
  );
  $result = drupal_http_request("http://namedayreminder.com/api_new/service.php", $options);
  $response = $result->data;
  $api_obj = json_decode($response, TRUE);
  if (isset($api_obj['error'])) {
    $error = 1;
    $message = $api_obj['error']['message'];
    if ($api_key!='') {
      drupal_set_message($message, 'warning', FALSE);
    }
    drupal_set_message(t('Get API Key <a href="http://www.namedayreminder.com/get-api/" target="_blank">here!</a>!'), 'warning', FALSE);
  }
  $postdata = http_build_query(array('body' => '{"jsonrpc" : "2.0","method" : "GetCountryList","params" : {"user" : "","domain" : "' . $domain . '","apiKey" : "' . $api_key . '"},"id" : "' . $uid . '"}'));
  $options = array(
    'method' => 'POST',
    'data' => $postdata,
    'timeout' => 15,
    'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
  );
  $result = drupal_http_request("http://namedayreminder.com/api_new/service.php", $options);
  $response = $result->data;
  $countries_obj = json_decode($response, TRUE);
  if (isset($countries_obj['result'])) {
    $countries_list = $countries_obj['result'];
  }
  else {
    $countries_list = array();
  }
  $countries = array();
  foreach ($countries_list as $ac_key => $ac_val) {
    foreach ($ac_val as $acc_key => $acc_val) {
      $countries[$acc_key] = $acc_val['country'];
    }
  }
  if (!empty($countries)) {
    if ($list_radio == 1) {
      $show_value = 'row';
    }
    else {
      $show_value = 'list';
    }
    $form['show'] = array(
      '#type' => 'radios',
      '#title' => t('Show name days'),
      '#default_value' => $show_value,
      '#description' => t('How to display name days.'),
      '#options' => array(
        'row' => t('Row'),
        'list' => t('List'),
      ),
    );  	
    $form['countries'] = array(
      '#title' => t('Choose country'),
      '#default_value' => $country,
      '#type' => 'select',
      '#default_value' => $country,
      '#options' => $countries,
    );
  }
  $form['#submit'] = array('nameday_form_submit');
  return system_settings_form($form);
}

/**
 * POSTS config form values and save in database.
 */
function nameday_form_submit($form, &$form_state) {
  $domain = $_SERVER['SERVER_NAME'];
  $domain = preg_replace('#^(http(s)?://)?w{3}\.#', '', $domain);
  $uid = 0;
  $values = $form_state['values'];
  $api_key = $form_state['values']['api_key'];
  if (isset($form_state['values']['countries'])) {
    $country = $form_state['values']['countries'];
  }
  else {
    $country = 'AT';
  }
  if (isset($form_state['values']['show'])) {
    $show = $form_state['values']['show'];
    if ($show == 'list') {
      $list = 1; $row = 0;
    }
    else {
      $list = 0; $row = 1;
    }
    if (!$country) {
      $country = 'AT';
    }
  }
  else {
    $list = 1; $row = 0;
  }
  $result = db_query('SELECT * FROM nameday LIMIT 1', array(':nid' => 1));
  if ($result->rowCount() > 0) {
    $query = db_update('nameday')
      ->fields(array(
        'api_key' => $api_key,
        'country' => $country,
        'show_row' => $row,
        'show_list' => $list)
      )
      ->condition('id', 1)
      ->execute();
  }
  else {
    $id = db_insert('nameday')
      ->fields(array(
        'api_key' => $api_key,
        'country' => $country,
        'show_row' => $row,
        'show_list' => $list)
      )
      ->execute(); 
  }
  $postdata = http_build_query(array('body' => '{"jsonrpc" : "2.0","method" : "CheckApiKey","params" : {"user" : "","domain" : "' . $domain . '","apiKey" : "' . $api_key . '"},"id" : "' . $uid . '"}'));
  $options = array(
    'method' => 'POST',
    'data' => $postdata,
    'timeout' => 15,
    'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
  );
  $result = drupal_http_request("http://namedayreminder.com/api_new/service.php", $options);
  $response = $result->data;
  $api_obj = json_decode($response, TRUE);
  if (isset($api_obj['error'])) {
    $error = 1;
    $message = $api_obj['error']['message'];
    drupal_set_message($message, 'warning', FALSE);
    drupal_set_message(t('Get API Key <a href="http://www.namedayreminder.com/get-api/" target="_blank">here!</a>!'), 'warning', FALSE);
  }
  else {
    $error = 0;
    $message = $api_obj['message'];
    drupal_set_message($message, 'status', FALSE);
  }
}
