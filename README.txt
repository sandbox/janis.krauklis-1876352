CONTENTS OF THIS FILE
Introduction
Installation
Usage
INTRODUCTION
Creator: janis.krauklis <http://drupal.org/user/2433232>

Module show`s name days for current day.

This module will do the following:

Check the api key. If api key is ok, then retrieve the list of name days.
Allows administrator to choose which country`s name days to show and choose how to choose them - in list or in one row.

INSTALLATION

1. Get api key: Go to http://www.namedayreminder.com/get-api/ and require api key
2. Download the module
3. Install and enable module "nameday"
4. Open configurations page "/admin/config/content/nameday"
5. Enter api key in API Key input field and click "Save configuration"
6. If everything is OK, then choose which country`s namedays you want to show

USAGE
Go to admin->structure->blocks and choose where to show block "Name days"
